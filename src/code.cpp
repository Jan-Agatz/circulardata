#include <Rcpp.h>
#include <math.h>

#define M_PI        3.141592653589793238462643383280    /* pi */

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

//' @export
// [[Rcpp::export]]
Rcpp::NumericVector nr_vm_cpp(int n, double mu=0, double kappa=1){
    Rcpp::NumericVector out(n);
    int i = 0;
    while (i != n){
        Rcpp::NumericVector U = Rcpp::runif(3);
        double a = 1 + sqrt(1+4*pow(kappa, 2));
        double b = (a-sqrt(2*a))/(2*kappa);
        double r = (1+pow(b, 2))/(2*b);
        double z = cos(M_PI*U[0]);
        double f = (1+r*z)/(r+z);
        double c = kappa * (r-f);
        if (c*(2-c) - U[1] > 0){
            out[i] = mu + sgn(U[2]-0.5) * acos(f);
            i++;
        }else if(log(c/U[1])+1-c < 0){
            continue;
        }else{
            out[i] = mu + sgn(U[2]-0.5) * acos(f);
            i++;
        }
    }


    return out;
}
