---
title: "estimations"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{estimations}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(CircularData)
```

The functions in the estimations.R script are designed to return the optimal 
parameters of the given circular distribution for any circular data.
We therefore simulate some arbitrary dataset:
```{r}
data <- sim(distribution="r_vm", n=1000, mu=pi, kappa=1/2)
```

The idea is as follows:
Since any distribution can be described by certain parameters, 
estimations calculates these parameters. This can be used for illustratory 
purposes:
```{r}
vmises <- estimate_rvm(data)
norm <- estimate_rwnorm(data)
cauchy <- estimate_rcauchy(data)

as_vmises <- nr_vm(n=1000, mu=vmises$mu, kappa=vmises$kappa, discrete=TRUE)
as_norm <- convert(rw_norm(n=1000, mu=norm$mu, rho=norm$rho), round=TRUE)
as_cauchy <- convert(rw_cauchy(n=1000, location=cauchy$location, scale=cauchy$scale), TRUE)

par(mfrow=c(2,2), mfcol=c(2,2))
circplot(data)
circplot(as_vmises)
circplot(as_norm)
circplot(as_cauchy)
```
Unfortunately, as of this point, only few distributions have been implemented.




