test_that("cData works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData)
  expect_identical(validData, cData(validData))

  expect_error(cData("False Tests"))

})

test_that("unit works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData)
  expect_equal(unit(validData), "rad")
  validData2 <- cData(rawData, unit = "deg")
  expect_equal(unit(validData2), "deg")
})

test_that("unit<- works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData)
  unit(validData) <- "deg"
  expect_equal(unit(validData2), "deg")
})

test_that("modulo works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData)
  validData <- convert(validData)
  expect_equal(modulo(validData), 360)
})

test_that("modulo<- works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData)
  validData <- convert(validData)
  modulo(validData) <- 720
  expect_equal(modulo(validData), 720)
})

test_that("start works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData)
  expect_equal(start(validData), 0)
})

test_that("start<- works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData)
  start(validData) <- 0.4
  expect_equal(start(validData), 0.4)
})

test_that("clockwise works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData)
  expect_equal(clockwise(validData), FALSE)
})

test_that("clockwise<- works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData)
  clockwise(validData) <- TRUE
  expect_equal(clockwise(validData), TRUE)
})

test_that("convert works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData)
  expect_equal(convert(validData), convert(convert(convert(validData))))
  expect_equal(modulo(convert(validData)), 360)
  expect_equal(unit(convert(validData)), "deg")
})

test_that("addition works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData, unit = "deg")
  expect_equal(validData, (validData + 1) + (-1))
})

test_that("is_cData works", {
  rawData <- tibble(direction = 1:20, n = 20:1)
  validData <- cData(rawData, unit = "deg")
  expect_equal(is_cData(validData), TRUE)
  expect_equal(is_cData("Nicht cData"), FALSE)
})
